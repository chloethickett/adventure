# attempt at text adventure
# by Chloe Thickett

import string as st


class Inventory:
    inventory_display = 4  # the number of items listed in each row, might try text wrap in future

    def __init__(self):
        self.inventory = []         # inventory list
        self.inventory_dict = {}    # inventory dictionary

    def display(self):
        """Displays the current inventory."""
        if len(self.inventory) == 0:  # empty inventory
            print('\nYour inventory is empty.\n')
        else:
            print('\nYour inventory:')
            for i in range(len(self.inventory)):
                print(st.capwords(self.inventory[i]), end='')
                if i == len(self.inventory) - 1:
                    print('.\n')
                elif (i + 1) % self.inventory_display == 0:
                    print(',')
                else:
                    print(',', end=' ')

    def add(self, item):
        """Appends item to the inventory."""
        self.inventory.append(item.names[0])
        for name in item.names:
            self.inventory_dict[name] = item
        print(item.names[0].capitalize() + ' was added to your inventory.\n')

    def remove(self, item):
        """Removes item from the inventory"""
        for name in item.names:
            del self.inventory_dict[name]
        self.inventory.remove(item.names[0])
        print(item.names[0].capitalize() + ' was removed from your inventory.\n')


class Location:
    """The locations items or the player can be in."""
    def __init__(self, name, look_around_text):
        self.item_dict = {}     # dictionary of accessible items from the location
        self.item_list = []     # list of distinct accessible items from the location
        self.name = name        # name of the location to be used for doors, must be unique for each location
        self.look_around_text = look_around_text    # string with initial description of the room

    def __str__(self):
        return self.name

    def look_around(self):
        print('\n' + self.look_around_text, end='')
        for item in self.item_list:
            item.look_around_()
        print('\n')

    def add_(self, item):
        """Adds item to the location, doesn't change item.locations"""
        for name in item.names:
            self.item_dict[name] = item
        self.item_list.append(item)

    def add(self, item):
        """Adds item to the location, changes item.locations"""
        self.add_(item)
        item.locations.append(self)

    def remove_(self, item):
        """Removes item from the location, doesn't changes item.locations"""
        for name in item.names:
            del self.item_dict[name]
        self.item_list.remove(item)

    def remove(self, item):
        """Removes item from the location, changes item.locations"""
        self.remove_(item)
        item.locations.remove(self)


class Player:
    """Information about the player"""
    def __init__(self, inventory, current_location):
        self.inventory = inventory
        self.current_location = current_location

    def available_items(self):
        available_items = {}
        available_items.update(self.inventory.inventory_dict)
        available_items.update(self.current_location.item_dict)
        return available_items


class InteractiveItem:
    """Items that can be interacted with by the player.
        Define a class that inherits from this if it can be used either on its own or with something."""
    def __init__(self,
                 names,                 # list of the possible names of an item, names[0] is the inventory name,
                                        #   names[-1] is the unique name used to define it.
                 locations,             # list of locations accessible from
                 player,                # player the item will interact with
                 status,                # the status of the object
                 look_around_text,      # dictionary of the message when the location is looked around for each status
                 look_at_text,          # dictionary of the message when looked at for each status
                 can_pick_up=False,     # whether the item can be picked up
                 can_open=False,        # whether the item can be opened
                 can_close=False):      # whether the item can be closed

        self.names = names
        self.locations = locations
        for location in self.locations:
            location.add_(self)
        self.player = player
        self.status = status
        self.look_around_text_ = look_around_text
        self.look_at_text = look_at_text
        self.can_pick_up = can_pick_up
        self.can_open = can_open
        self.can_close = can_close

    def __str__(self):
        return self.names[-1]

    def look_around_(self):
        print(self.look_around_text_[self.status], end='')

    def look_at(self):
        print('\n' + self.look_at_text[self.status] + '\n')

    def pick_up(self):
        if self.can_pick_up:
            print('\nYou picked up the ' + self.names[0] + '.')
            self.player.inventory.add(self)
            for location in self.locations:
                location.remove_(self)
            self.locations = []
            self.can_pick_up = False
        else:
            print("\nYou are unable to pick up that up.\n")

    def open(self):
        if self.can_open:
            if self.status == 'closed':
                self.status = 'open'
                print('\nThe ' + self.names[0] + ' opened.\n')
            elif self.status == 'open':
                print("\nIt's already open.\n")
            elif self.status == 'locked':
                print("\nIt's locked. You can't open it.\n")
            else:
                print("""\nYOU SHOULDN'T BE HERE, 
                      the only 2 statuses for this item can be 'open', 'closed' and 'locked'.\n""")
        else:
            print("\nYou can't open that.\n")

    def close(self):
        if self.can_close:
            if self.status == 'open':
                self.status = 'closed'
                print('\nThe ' + self.names[0] + ' closed.\n')
            elif self.status == 'closed' or self.status == 'locked':
                print("\nIt's already closed.\n")
            else:
                print("""\nYOU SHOULDN'T BE HERE, 
                      the only 2 statuses for this item can be 'open', 'closed' and 'locked'.\n""")
        else:
            print("\nYou can't close that.\n")

    def use(self, item=None):
        if item is None:
            print("\nYou can't use that.\n")
        else:
            print("\nYou can't use that there.\n")


class Container(InteractiveItem, Location):
    """An InteractiveItem that also functions as a Location as it can contain other InteractiveItems."""
    def __init__(self,
                 names,                 # list of the possible names of an item, names[0] is the inventory name,
                                        #   names[-1] is the unique name used to define it.
                 locations,             # list of locations accessible from
                 player,                # player the item will interact with
                 status,                # the status of the object
                 look_around_text,      # dictionary of the message when the location is looked around for each status
                 look_at_text,          # dictionary of the message when looked at for each status
                 can_pick_up=False,     # whether the item can be picked up
                 can_close=True):       # whether the item can be closed
        InteractiveItem.__init__(self,
                                 names,
                                 locations,
                                 player,
                                 status,
                                 look_around_text,
                                 look_at_text,
                                 can_pick_up=can_pick_up,
                                 can_open=True,             # you must be able to open a container
                                 can_close=can_close)
        Location.__init__(self, self.names[-1], '')
        self.contents_display = 4

    def look_around(self):
        print("\nSomehow you appear to be in a container, HOW DID YOU GET HERE?")
        self.look_at()

    def look_at(self):
        print('\n' + self.look_at_text[self.status])
        if self.status == 'open':
            if len(self.item_list) == 0:  # container is empty
                print('The ' + self.names[0] + ' is empty.\n')
            else:
                print('The ' + self.names[0] + ' contains:')
                for i, item in enumerate(self.item_list):
                    print(item.names[0].capitalize(), end='')
                    if i == len(self.item_list) - 1:
                        print('.\n')
                    elif (i+1) % self.contents_display == 0:
                        print(',')
                    else:
                        print(',', end=' ')
        else:
            print()

    def open(self):
        if self.status == 'closed':
            self.status = 'open'
            for location in self.locations:
                for item in self.item_list:
                    location.add(item)
            print('\nThe ' + self.names[0] + ' opened.\n')
        elif self.status == 'open':
            print("\nIt's already open.\n")
        elif self.status == 'locked':
            print("\nIt's locked. You can't open it.\n")
        else:
            print("""\nYOU SHOULDN'T BE HERE, 
                  the only 2 statuses for this item can be 'open', 'closed' and 'locked'.\n""")

    def close(self):
        if self.can_close:
            if self.status == 'open':
                self.status = 'closed'
                for location in self.locations:
                    for item in self.item_list:
                        location.remove(item)
                print('\nThe ' + self.names[0] + ' closed.\n')
            elif self.status == 'closed' or self.status == 'locked':
                print("\nIt's already closed.\n")
            else:
                print("""\nYOU SHOULDN'T BE HERE, 
                      the only 2 statuses for this item can be 'open', 'closed' and 'locked'.\n""")
        else:
            print("\nYou can't close that.\n")


class Door(InteractiveItem):
    """Item that changes the players location when used."""
    def __init__(self,
                 names,             # list of the possible names of an item, names[0] is the inventory name,
                                    #   names[-1] is the unique name used to define it.
                 locations,         # list of locations accessible from
                 location_map,      # dictionary of the current location and where it takes you
                 player,            # player the item will interact with
                 status,            # the status of the object
                 look_around_text,  # dictionary of the message when the location is looked around for each status,
                                    # and each location so would look something like this:
                                    # {'location1': {'open': "...", 'closed': "...", 'locked': "..."},
                                    #  'location2': {'open': "...", 'closed': "...", 'locked': "..."}}
                 look_at_text,      # dictionary of the message when looked at for each status
                 can_pick_up=False,     # whether the item can be picked up
                 can_open=True,         # whether the item can be opened
                 can_close=True):       # whether the item can be closed
        InteractiveItem.__init__(self,
                                 names,
                                 locations,
                                 player,
                                 status,
                                 look_around_text,
                                 look_at_text,
                                 can_pick_up=can_pick_up,
                                 can_open=can_open,
                                 can_close=can_close)
        self.location_map = location_map

    def look_around_(self):
        print(self.look_around_text_[self.player.current_location.name][self.status], end='')

    def use(self, item=None):
        if item is None:
            if self.status == 'locked':
                print("\nIt's locked. You can't use it at the moment.\n")
            elif self.status == 'open' or self.status == 'closed':
                self.player.current_location = self.location_map[str(self.player.current_location)]
                self.player.current_location.look_around()
            else:
                print("""\nYOU SHOULDN'T BE HERE, 
                                      the only 2 statuses for this item can be 'open', 'closed' and 'locked'.\n""")
        elif type(item) is Key:
            item.use(self)
        else:
            print("\nYou can't use that there.\n")


class Key(InteractiveItem):
    """Item that will unlock other items, the initial unlocked status of the partner_item is 'closed'."""
    def __init__(self,
                 names,                 # list of the possible names of an item, names[0] is the inventory name,
                                        #   names[-1] is the unique name used to define it.
                 locations,             # list of locations accessible from
                 player,                # inventory the item will interact with
                 status,                # the status of the object
                 look_around_text,      # dictionary of the message when the location is looked around for each status
                 look_at_text,          # dictionary of the message when looked at for each status
                 partner_item,          # item it unlocks
                 can_pick_up=True,      # whether the item can be picked up
                 can_open=False,        # whether the item can be opened
                 can_close=False):      # whether the item can be closed

        InteractiveItem.__init__(self,
                                 names,
                                 locations,
                                 player,
                                 status,
                                 look_around_text,
                                 look_at_text,
                                 can_pick_up=can_pick_up,
                                 can_open=can_open,
                                 can_close=can_close)
        self.partner_item = partner_item

    def use(self, item=None):
        if item is None:
            print('\nYou need to use it with something.\n')
        elif item is self.partner_item:
            if self.partner_item.status is 'locked':
                self.partner_item.status = 'closed'
                print('\nThe ' + self.partner_item.names[0] + ' unlocked!\n')
                if self.locations == []:
                    self.player.inventory.remove(self)
                else:
                    for location in self.locations:
                        location.remove_(self)
            else:
                print("""\nYOU SHOULDN'T BE HERE, 
                        if the door is unlocked but you have the key, the status has gone wrong somewhere.\n""")
        else:
            print("\nYou can't use that there.\n")


class Game:
    verb_list = ['loo', 'pic', 'tak', 'get', 'ope', 'clo', 'use']

    def __init__(self, player, end_location=None):
        self.last_print = ''  # the last written message
        self.player = player
        self.command_dict = {
                             'exit': self.leave_game, 'quit': self.leave_game,
                             'help': self.help, 'h': self.help,
                             'inventory': self.player.inventory.display, 'i': self.player.inventory.display,
                             'look around': self.player.current_location.look_around,
                             'look': self.player.current_location.look_around
                            }
        self.end_location = end_location
        self._command_items = []

    @staticmethod
    def confirm(string, message=''):
        """Allows you to confirm doing something."""
        while True:
            answer = input('\nAre you sure you want to %s? %s\n[yes] or [no]\n' % (string, message)).casefold().strip()
            if answer == 'yes':
                return True
            elif answer == 'no':
                return False
            else:
                print('You must answer yes or no.')

    @staticmethod
    def help():
        """Help message."""
        print("\nTo play you need to type in commands such as 'look at box' or 'use key with door'.\n"
              "These commands consist of a verb and an item you want to interact with.\n"
              "The verbs are: 'look at', 'pick up', 'open', 'close', and 'use'.\n"
              "'Use' is special since you can also use an item 'with' another item like in the example above.\n"
              "You can also 'look around' a room or for short simply use 'look'.\n"
              "Look at your inventory with the command 'inventory' or for short 'i'.\n"
              "You can leave the game with 'exit' or 'quit'.\n"
              "Finally, if you want to see this again simply type 'help' or just 'h'.\n")

    def welcome_message(self):
        """The message at the start of the game"""
        print("\nWelcome to my small game, it won't take you long to complete, its very simple.\n"
              "If I was to take this further, this would probably be the very easy bit at the beginning,\n"
              "so the tutorial, and I would have puzzles or maybe even try to incorporate the maze mechanics\n"
              "into it somehow. But that's enough talk, lets get into it!")
        self.help()
        print("Now you are ready to begin!")
        self.player.current_location.look_around()

    def leave_game(self):
        """Exits the game"""
        yn = self.confirm('quit')
        if yn:
            raise SystemExit
        else:
            print(self.last_print)

    def _item_search(self, command=' ', no_items=1):
        x = 0
        self._command_items = []
        for index, item in self.player.available_items().items():
            if index in command:
                self._command_items.append(item)
                x += 1
            if x == no_items:
                break
        if x != no_items:
            if command[:3] in self.verb_list:
                print("\nYou can't do that.\n")
            else:
                print("\nNot a valid input\n")
            return False
        else:
            return True

    def tick(self):
        """One tick of game time."""
        if self.player.current_location is self.end_location:
            return True
        command = input().casefold().strip()  # player input
        try:
            self.command_dict[command]()
        except KeyError:
            if command[:3] == 'use' and ('with' in command or ' in ' in command or ' on ' in command):
                valid_items = self._item_search(command, 2)
                if valid_items:
                    try:
                        self._command_items[0].use(self._command_items[1])
                    except KeyError:
                        print("\nNot a valid input\n")
            else:
                valid_items = self._item_search(command)
                if valid_items:
                    verb_dict = {'loo': self._command_items[0].look_at,
                                 'pic': self._command_items[0].pick_up,
                                 'tak': self._command_items[0].pick_up,
                                 'get': self._command_items[0].pick_up,
                                 'ope': self._command_items[0].open,
                                 'clo': self._command_items[0].close,
                                 'use': self._command_items[0].use}
                    try:
                        verb_dict[command[:3]]()
                    except KeyError:
                        print("\nNot a valid input\n")
        return False

    def run(self):
        """Runs the game."""
        self.welcome_message()
        while True:
            if self.tick():
                break


if __name__ == "__main__":
    inventory_ = Inventory()
    room = Location('room', "It's a dark room without any windows, ")
    room2 = Location('room2', "Congratulations! You completed the short game!")
    player_ = Player(inventory_, room)
    ceiling_light = InteractiveItem(['light', 'lamp'],
                                    [room],
                                    player_,
                                    'neutral',
                                    {'neutral': "with the only light coming from a small ceiling light.\n"
                                                "The light isn't much, but it is enough to see what else "
                                                "is in the room, and lack the of a light switch."},
                                    {'neutral': "It's a small ceiling light, it's a bit dim and flickery,\n"
                                                "but enough to see what is in the room.\n"
                                                "There is no sign of a light switch,\n"
                                                "so you can't plummet yourself into darkness."})
    door = Door(['door'],
                [room, room2],
                {'room': room2, 'room2': room},
                player_,
                'locked',
                {'room': {'locked': "\nOn one wall there is a wooden door, it looks old but sturdy and firmly shut.",
                          'closed': "\nOn one wall there is a wooden door, it looks old but sturdy.",
                          'open': "\nOn one wall there is a widely open wooden door."},
                 'room2': {'locked': "",
                           'closed': "",
                           'open': ""}},
                {'locked': "It's a wooden door, it looks old but sturdy and firmly shut.\n"
                           "Between the small gap of the door and the frame you can see the latch of the lock.\n"
                           "If you want to get through, you will need a key.",
                 'closed': "It's a wooden door, it looks old but sturdy and firmly shut, but not locked.",
                 'open': "It's a wooden door that looks old and is wide open."})
    table = InteractiveItem(['table'],
                            [room],
                            player_,
                            'neutral',
                            {'neutral': "\nIn one corner of the room there is a rickety table, "},
                            {'neutral': "It's a rickety wooden table with carvings down each leg,\n"
                                        "they are a bit worn but the detail is still extravagant.\n"
                                        "Sat on top of it is a small wooden box, that has been nailed in place."})
    box = Container(['box', 'chest'],
                    [room],
                    player_,
                    'closed',
                    {'closed': "and on it is sat a box,\nonly a small wooden box with carved etching on the lid.",
                     'open': "and on it is sat an open box,\nonly a small wooden box"},
                    {'closed': "It's a small wooden box with carved etchings on the lid,\n"
                               "it has a floral pattern with roses and foxgloves, and clearly lovingly made.\n"
                               "Around the sides of the box it has keys painted on,\n"
                               "and around the base there are nails going in at diagonals where\n"
                               "it has been nailed to the table it is sat on.",
                     'open': "It's a small wooden open box with carved etchings on the lid that has been\n"
                             "removed from the base, it has a floral pattern with roses and foxgloves,\n"
                             "and clearly lovingly made. Around the sides of the box it has keys painted on,\n"
                             "and around the base there are nails going in at diagonals where\n"
                             "it has been nailed to the table it is sat on."})

    key = Key(['key'],
              [box],
              player_,
              'neutral',
              {'neutral': "with a rusty old key inside."},
              {'neutral': "It's a rusty old key."},
              door)
    game = Game(player_, end_location=room2)
    game.run()
